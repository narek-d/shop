<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use function MongoDB\BSON\toJSON;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.app');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data= $request->all();
        if(Product::create($data)){
            return response(['message' => 'Product was created'],200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $data = $request->all();
        if($product->update($data)){
            return response(['message' => 'Product was updated'],200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        if($product->delete()){
            return response(['message' => 'Product was deleted'],200);
        }
    }

    public function getProducts(Request $request)
    {
        $column = 'id';
        $order_by = 'DESC';

        if(!is_null($request->params['column'])){
            if($request->params['trigger'] == '0'){
                $column = $request->params['column'];
                $order_by = 'ASC';
            }
            else{
                $column = $request->params['column'];
                $order_by = 'DESC';
            }
            $products = Product::orderBy($column, $order_by)->paginate($request->params['limit']);
        }

        $products = Product::orderBy($column, $order_by)->paginate($request->params['limit']);
        return response()->json($products);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchProducts(Request $request)
    {
        if(!empty($request->params['searchVal'])){

            $products = Product::where('name', 'LIKE', '%'.$request->params['searchVal'].'%')->paginate($request->params['limit']);
            $products = $products->toArray();

        }
        else{
            $products = [];
        }

        return response()->json($products);

    }

}
