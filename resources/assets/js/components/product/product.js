export default {
    data(){
        return{
            id: '',
            index: '',
            isVisible: false,
            visibleEdit:false,
            products: [],
            name:'',
            price:'',
            pagination: [],
            currentUrl: '/',
            limit: 5,
            trigger: 0,
            idIconClass: true,
            searchString: '',
            url: '/get-products?page='+localStorage.page,
            result: false
        }
    },

    mounted(){
        this.getProducts();
    },

    methods:{
        getProducts(select = null, column = null, trigger = null){
            let vm = this;

            select &&  localStorage.setItem("limit", this.limit);
            this.limit = localStorage.limit;
            localStorage.setItem("page", 1);

            axios.post(this.url,{
                params: {
                    limit: vm.limit,
                    column: column,
                    trigger: trigger,
                }
            })
                .then((response) => {
                    if(select){
                        location.href = '/';
                    }
                    vm.products = response.data.data;
                    vm.makePagination(response.data);
                })
                .catch(function (error) {

                });
        },

        passDeleteData(product,index){
            this.isVisible = true;
            this.id = product.id;
            this.index = index;
        },

        passEditData(product,index){
            this.visibleEdit = true;
            this.id = product.id;
            this.index = index;
            this.name = product.name;
            this.price = product.price;
        },

        async deleteProduct(id,index){
            let vm = this;
            await axios.delete('/product/' + id)
                .then((response) => {
                    if(response.status === 200){
                        this.isVisible = false;
                        this.products.splice(index, 1);
                        this.products.length === 0 && document.getElementById('prev').click();
                    }
                })
                .catch(function (error) {
                    //
                });
        },

        editProduct(id,index){
            let vm = this;
            axios.put('/product/' + id, {
                name: vm.name,
                price: vm.price
            })
                .then((response) => {
                    if(response.status === 200){
                        this.visibleEdit = false;
                        this.products[index] = {id: id, name: vm.name, price: vm.price};
                    }
                })
                .catch(function (error) {
                    //
                });
        },

        makePagination(data){

            let pagination = {
                current_page: data.current_page,
                last_page: data.last_page,
                next_page_url: data.next_page_url,
                prev_page_url: data.prev_page_url,
            };

            this.pagination = pagination;
            history.pushState(null, null, '/#/page/' + this.pagination.current_page);
            this.searchString === '' && this.pagination.last_page >= this.pagination.current_page && localStorage.setItem("page", this.pagination.current_page);
        },

        fetchPaginateProducts(url){
            if(this.searchString === ''){
                this.url = url;
                this.getProducts();
            }
            else{
                this.searchProducts(url);
            }
        },

        orderBy(column){
            this.getProducts(null, column, this.trigger);
            this.trigger = (this.trigger === 0) ? 1 : 0;
            this.idIconClass = (this.idIconClass !== true);
        },

        async searchProducts(url = null){
            if(!url){
                url = '/search';
            }
            let vm = this;
            await axios.post(url,{
                params:{
                    searchVal: this.searchString,
                    limit: this.limit,
                }
            })
                .then( response => {
                    if(this.searchString !== ''){
                        if(response.data.data.length === 0){
                            this.products = [];
                            vm.result = true;
                        } else{
                            vm.result = false;
                            this.products = response.data.data;
                            this.makePagination(response.data)
                        }
                    }else{
                        if(response.data.length === 0){
                            vm.getProducts();
                            vm.result = false;
                        }
                    }
                })
                .catch(function (error) {
                    //
                });
        },

    },
}
