import AddProduct from './components/AddProductComponent.vue'
import ProductTable from './components/product/product.vue'

export const routes = [
    { path: '/', component: ProductTable },
    { path: '/add-product', component: AddProduct },
    { path: '/page/:num', component: ProductTable },
];