import Vue from 'vue'
import axios from 'axios'
import VueRouter from 'vue-router'
import VuePaginate from 'vue-paginate'

import { routes } from './routes'

Vue.use(VueRouter);
Vue.use(VuePaginate);

require('./bootstrap');
// window.axios = require('axios');
// window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
// Vue.prototype.$http = window.axios;

const router = new VueRouter({
    routes
});

const app = new Vue({
    router
}).$mount('#app');
