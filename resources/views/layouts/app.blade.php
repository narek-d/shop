<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>LearnVue</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="css/bulma.min.css" rel="stylesheet" type="text/css">
    <link href="css/main.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet"
          type="text/css">

    <!-- Styles -->
</head>
<body>

<div id="app" class="box container">
    <nav class="navbar is-primary" role="navigation" aria-label="dropdown navigation">
        <div class="navbar-start">
            <router-link class="navbar-item" to="/">Products</router-link>
        </div>
        <div class="navbar-end">
            <router-link class="navbar-item" to="/add-product"><button class="button is-info">Add Product</button></router-link>
        </div>
    </nav>
    <router-view></router-view>
</div>

</body>

{{--<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>--}}
{{--<script src="https://unpkg.com/vue-router/dist/vue-router.js"></script>--}}
{{--<script src="https://unpkg.com/axios/dist/axios.min.js"></script>--}}
{{--<script src='js/app.js' type="text/javascript"></script>--}}
<script src="{{ asset('js/app.js') }}" defer></script>
{{--<script>--}}
{{--$.ajaxSetup({--}}
{{--headers: {--}}
{{--'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
{{--}--}}
{{--});--}}
{{--</script>--}}
</html>